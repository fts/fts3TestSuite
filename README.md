## Description

Pre-production tests for providing load and stress tests in the FTS pilot at CERN. 
More information: https://fts3-docs.web.cern.ch/fts3-docs/docs/developers/qa.html


## Setup

### Master

Configuration regarding commands, machines and processes to run is set in masterSlave/config.py

### Slaves

For specific configuration of the tests, you must go to the config.py for each test:
- doppelgangerTest/config.py
- optimizerTest/config.py
- activitySharesTest/config.py
- dbScalabilityTest/config.py
- stagingTest/config.py


## Usage
	
To run the tests you just need to run: python testSubmitMaster.py
