#!/usr/bin/env python
# Generate some commands.
# The extra double quotes in this commented-out set created problems:  the host
# thought the command to execute was "./testScript 'testing'" with the quotes,
# but no such file or directory is found.
# commands = ["\"./testScript 'Testing Process number %i'" % (number) + "\" &> output%i.out" % (number) \
#            for number in range(4)]
# This became wrong when I stopped using shlex to split the command in
# JobDistributor.


import os
from masterSlave.submitMaster import *


ProcessNumber = config.MaxProcesses
Nodes = len(config.Machines)

"""
Get current dir, so we can we can cd to that in our command.
Again, as described in submitMaster and JobDistributor, it is
our responsibility to generate the line as it should be executed
on the host machines.
"""
#curDir = os.path.abspath('.')
curDir = '/afs/cern.ch/user/f/ftssuite/public/fts3TestSuite'
cmds =[]
total_range = Nodes * ProcessNumber

print str(Nodes)
print str(ProcessNumber)

for cmd in config.Commands:
    command =["cd %s; python %s %i %i 'Testing Process number %i'" % (curDir, cmd, number, total_range, number)  # \
              
            for number in range(1, (Nodes*ProcessNumber)+1)]
    cmds = command + cmds
           
print cmds
processCommandsInParallel(cmds)   

