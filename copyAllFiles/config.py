import datetime
import os


PublicCert = os.environ.get('X509_USER_CERT', '/afs/cern.ch/user/f/ftssuite/.globus/usercert.pem')
PrivateKey = os.environ.get('X509_USER_KEY', '/afs/cern.ch/user/f/ftssuite/.globus/userkey.pem')
CaPath = '/etc/grid-security/certificates'

FTS3Host = os.environ.get('FTS3_HOST', 'fts3-devel.cern.ch')
FTS3Port = 8446

#logging path
logpath = '/afs/cern.ch/user/f/ftssuite/public/fts3TestSuite/logging.conf'

submissions = 2000
timeout = 3600*3



source = 'gsiftp://eospublicftp.cern.ch/eos/ftstest/'
#destinations = ['srm://f-dpm001.grid.sinica.edu.tw:8446/srm/managerv2?SFN=/dpm/grid.sinica.edu.tw/home/dteam/', 
 #               'srm://srm.pic.es:8443/srm/managerv2?SFN=/pnfs/pic.es/data/dteam/',
destinations = ['srm://srm-cms.jinr-t1.ru:8443/srm/managerv2?SFN=/pnfs/jinr-t1.ru/data/dteam/',
                'srm://ccsrm.in2p3.fr:8443/srm/managerv2?SFN=/pnfs/in2p3.fr/data/dteam/',
                'srm://gridka-dCache.fzk.de:8443/srm/managerv2?SFN=/pnfs/gridka.de/dteam/',
                'srm://srm.grid.sara.nl:8443/srm/managerv2?SFN=/pnfs/grid.sara.nl/data/dteam/',
                's3://ftstests.cs3.cern.ch/',
                'srm://storm02.clumeq.mcgill.ca:8444/srm/managerv2?SFN=/dteam/']



