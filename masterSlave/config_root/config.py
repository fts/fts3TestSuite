import datetime
import os



##Config jobs

MaxProcesses = 20
#Machines = ['ftstest002.cern.ch, ftstest004.cern.ch, ftstest005.cern.ch,ftstest006.cern.ch, ftstest007.cern.ch, ftstest008.cern.ch, ftstest009.cern.ch, ftstest010.cern.ch, ftstest011.cern.ch, ftstest012.cern.ch, ftstest013.cern.ch','ftstest014.cern.ch', 'ftstest015.cern.ch','ftstest020.cern.ch','ftstest017.cern.ch', 'ftstest018.cern.ch', 'ftstest019.cern.ch']
Machines = ['ftstest011.cern.ch', 'ftstest012.cern.ch', 'ftstest013.cern.ch','ftstest014.cern.ch', 'ftstest015.cern.ch','ftstest016.cern.ch','ftstest017.cern.ch', 'ftstest018.cern.ch', 'ftstest019.cern.ch', 'ftstest020.cern.ch']

user = 'ftssuite'

#logging path
logpath = '/afs/cern.ch/user/f/ftssuite/public/fts3TestSuite/logging.conf'
logrotate = '/afs/cern.ch/user/f/ftssuite/public/fts3TestSuite/ftssuite.logrotate'
Commands=['rootTest/root.py']
