import datetime
import os



##Config jobs

MaxProcesses = 1
Machines = ['ftstest001.cern.ch']

user = 'ftssuite'

#logging path
logpath = '/afs/cern.ch/user/f/ftssuite/public/fts3TestSuite/logging.conf'
logrotate = '/afs/cern.ch/user/f/ftssuite/public/fts3TestSuite/ftssuite.logrotate'
Commands=['stagingTest/staging.py']

