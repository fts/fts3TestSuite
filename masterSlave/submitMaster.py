#!/usr/bin/env python
"""
This defines the submitMaster thread using the JobDistributor.
This process should be started by the script that generates commands.
"""

#from multiprocessing import Connection
from listQueue import listQueue
from multiprocessing import Process, Pipe
import threading
from jobDistributor import *
import logging
import logging.config
import logging.handlers
import os, functools, sys, time
import trollius as asyncio
import random
import config
import getpass,socket
import signal


logger = logging.getLogger('master')
pconn, cconn = Pipe()
p = threading.Thread()
run = True


def signal_handler(signal, frame):
    global run
    
    pconn.send('quit')
   
    time.sleep(10)
    if pconn.recv() == 'quit':
       
        p.join()
        logger.info('Cancel tests.')
        run = False
        sys.exit(0)
        
def submitMaster(conn):
    logger.info("submit Master started...")
    JD = JobDistributor(logger)
    jobQueue = listQueue(10)

    logger.info("Job Distributor started, with %i nodes, %i jobs possible" % \
          JD.info())

    while True:
        #See if there is something to do.
        if conn.poll():
            command = conn.recv()
            if command.lower() == 'sent':
                
                if len(JD) == 0 and jobQueue.isEmpty():
                    conn.send('yes')
                    conn.close()
                    return
                else:
                    conn.send('no')
                    
                    
            else:
                if command.lower() == 'quit':
                   
                    for host in config.Machines:
                        for job in JD.processes[host]:
                            
                            job.terminate()
                    conn.send('quit')
                    conn.close()
                    return
                else:
                    jobQueue.enqueue(command)

        #Distribute as many jobs as possible.
        
        while not jobQueue.isEmpty() and not JD.isFull():
            JD.distribute(jobQueue.dequeue())   
            
        if not conn.poll():
            logger.info('Going to sleep...')
            return
            

"""
Convenience function for the caller.  Every caller basically wants this:
send in jobs, wait til they're finished.
"""
def processCommandsInParallel(commands, ):
    global pconn, cconn, p
    
    
    logging.config.fileConfig(config.logpath)
    signal.signal(signal.SIGINT, signal_handler)
    
    # We now hang around for the workers to finish their work.
    
    
    p = threading.Thread(target=submitMaster, \
                    args=(cconn,))
    p.start()
    #Send the jobs in.
    for command in commands:
        pconn.send(command)
    #Don't quit until the submitMaster says it's done.
   
    
        