#!/usr/bin/env python
"""
A job distributor class, that maintains a list of available machines,
and distributes jobs to them. This class basically makes the system call
'ssh host command'.  It's the caller's responsibility to generate
appropriate command lines, redirecting output as they see fit.

uses subprocess to do this, instantiating a process to execute the
ssh command.  Process information is then stored in a Job object,
which can in turn be queried.

This class should be instantiated by an independent thread, which
periodically checks for the completion of jobs and maintains a job
queue.  See submitMaster and testSubmitMaster for guidance. When
presented with a job to distribute when all hosts are busy to their
capacity, this code will hang, waiting for a spot to open up.  It's
someone else's responsibility to keep the original caller informed
about the submissions made--again, see submitMaster.
"""

import os, sys, shlex, subprocess, time, logging
from listQueue import listQueue
import config
import multiprocessing, socket


#My computer list, should be able to ssh to without a password.

class Job(object):
    def __init__(self, host, user, proc, rpid, cmd_line, log):
        
        self.user = user
        self.host = self.user + host
        self.proc = proc
        self.rpid = rpid
        self.cmd_line = cmd_line
        self.startTime = time.ctime()
        self.logger = log
       
        

    def __str__(self):
        ret = '[Job on host %s: %-15s started %s]' % \
              (self.host, self.cmd_line[:15], self.startTime)
        return ret

    def poll(self):
        #return None for testing
        return None
        #return self.proc.poll()
    def terminate(self):
       
        command = 'kill -TERM -'+str(self.rpid)
        p = subprocess.Popen(['ssh', '-K', '-o', 'StrictHostKeyChecking=no', self.host, command])        
        self.logger.info('Killing process ' + self.rpid + ' in ' + self.host)

class JobDistributor(object):
    #Some static members.  Replace the elements of 
    #computer_list with hostnames you have ssh access to
    #without a password (see ssh-keygen)
    computer_list = config.Machines
    logger = logging
    maxJobs = config.MaxProcesses
    user = config.user
    processes = {}  
    #dictionary associating hostname to a list of Job objects.
    totalJobs = 0
    instances = 0
    
    def __init__(self, logging):
        
        if self.instances == 1:
            raise AssertionError('JobDistributor init ERROR: ' + \
                                 'Only one JD object allowed')
        self.instances = 1
        self.logger = logging
        
        for host in self.computer_list:
            self.processes[host] = []
        self.cleanComputerList()

    def setMaxJobs(self, num):
        self.maxJobs = num

    def isFull(self):
        return len(self) == len(self.processes)*self.maxJobs

    def info(self):
        return (len(self.processes), self.maxJobs*len(self.processes))
    
    def distribute(self, command):
        procNum = self.totalJobs
        #Simplified for now.  Just to see if the data all works.
        self.logger.info('\nSearching for host for process %i...' % (procNum))
        hostFound = False
        waitCycles = 0
        while not hostFound:
            try:
                host = self.getHost()
                self.logger.info('Host %s chosen for proc %i.' % (host, procNum))
                hostFound = True
                break
            except ValueError:
                self.logger.error('%sWaiting to find host for proc %i.' % \
                      ('.'*waitCycles, procNum))
                waitCycles += 1
                time.sleep(waitCycles)
                #The exception here should not happen, because the
                #submitMaster that calls this ensures there is
                #availability.  This will hang if it was mistaken.
        self.logger.info('Starting command.')
        command = "echo $$; "+command
        try:
            p = subprocess.Popen(['ssh', '-K', '-o', 'StrictHostKeyChecking=no',host, command], stdout=subprocess.PIPE, preexec_fn=os.setsid)
            
            for line in iter(p.stdout.readline, b''):
                self.rpid = line.split('\n')[0]
                break
                
            self.logger.info('Submited to ' + host + ': ' + command+ ' remote pid '+ self.rpid)
            host = host.split("@",1)[1]
            self.processes[host].append(Job(host, self.user, p, self.rpid, command, self.logger))
            self.totalJobs += 1
        except Exception as e:
            self.logger.error('Error running command %s' % str(e))

        
    def getHost(self):
        """Find a host among the computer_list whose load is less than maxJobs."""
        #Could loop through computer_list here, but computer_list still lists the
        #unavailable ones.  
        for host in self.processes:
            #clean out finished jobs. Keep only those which haven't terminated.
            self.processes[host] = [p for p in self.processes[host] if p.poll() is None]

            if len(self.processes[host]) < self.maxJobs:
                host = self.user+'@'+ host
                try:
                    if subprocess.check_call(shlex.split('ssh -K -o StrictHostKeyChecking=no ' + host + ' date'), \
                                     stdout=subprocess.PIPE) == 0:
                        return host
                except subprocess.CalledProcessError:
                    self.logger.error('getHost() error: host %s not available.' % (host))
                    raise ValueError('getHost() failed: could not find host.')

    def __str__(self):
        return '[JobDistributor: %i jobs on %i hosts]' % \
               (len(self), len(self.processes))
    
    def __len__(self):
        #Return number of jobs running
        return self.cleanup()

    def cleanup(self):
        for host in self.processes:
            #clean up finished processes.
            self.processes[host] = [j for j in self.processes[host] if j.poll() is None]
        return sum([len(plist) for plist in self.processes.values()])

    def cleanComputerList(self):
        for host in self.computer_list:
            host = self.user+'@'+ host
            try:
                subprocess.check_call(shlex.split('ssh -K -o StrictHostKeyChecking=no ' + host + ' date'), \
                                     stdout=subprocess.PIPE);
            except:
                self.logger.error('cleanComputerList: host %s deemed unavailable, ignoring...' \
                      % (host))
                host = host.split("@",1)[1]
                self.processes.pop(host);
    





    
    
