#!/usr/bin/env python
#
# Subscribe to the state change topic, submit to a different FTS3 the same
# job but with mock:// instead
# Can't really get the job, only the transfers. For the moment being, live with it
#


import logging
import logging.config
import os
import random
import subprocess
import sys
import urllib
import urlparse
import socket
import signal
import sys

import fts3.rest.client.easy as fts3


MB = 1024 * 1024
GB = MB * 1024
MaxFiles_1MB = 30
MaxFiles_1MB_1GB = 150
MaxFiles_1GB = 120
Transfers = 10


# Worker

 
class ActivityShares(object):
    """
    Mirrors requests
    """

    def __init__(self, config, id_process):
        """
        Constructor
        """
        
        self.logger = logging.getLogger('slave')
        self.job_ids = []
        self.register_signals()
        self.logger.error("pid: "+ str(os.getpid()))   
        self.id_process = id_process
        self.transfers = Transfers / self.id_process
        self.host = config.FTS3Host
        self.port = config.FTS3Port
        self.ca = config.CaPath
        self.fts3_context = fts3.Context(
                        endpoint='https://%s:%d' % (self.host, self.port),
                        capath=self.ca
                        )
        self.source = config.source
        self.dest = config.dest
        self.activity_queue = config.activity_queue
        self.files = config.files
        
        self.run()

    
    def register_signals(self):
        for i in [x for x in dir(signal) if x.startswith("SIG")]:
            try:
                signum = getattr(signal,i)
                signal.signal(signum, self.signal_handler)
            except Exception, e:
                self.logger.debug("Skipping "+ i)
                  
    def signal_handler(self, signal, frame):
        self.logger.debug("Signal %s", signal)
        fts3_context = fts3.Context(
                        endpoint='https://%s:%d' % (self.host, self.port),
                        capath=self.ca
                        )
        for id in self.job_ids:
            try:
                job_info = fts3.get_job_status(fts3_context, id, list_files=True)
                job_status = 'NONE'
                if job_info != None:
                    job_status = job_info['job_state']
                if job_status not in job_finished_status:
                    fts3.cancel(fts3_context, id)
                self.logger.info("Cancel job %s", id)
            except Exception, e:
                self.logger.error("Failed to cancel job %s with status %s: %s", id, job_status, str(e))
        sys.exit(0)
        
    
#     def error(self, *args):
#         self.logger.error( " ".join( map( str, args ) ) )
    
    def run(self): 
        for act in self.activity_queue:
            n = self.activity_queue[act]
            for i in range (0, n):
                f = self.files[random.randint(0, len(self.files)-1)]
                transfer = fts3.new_transfer(self.source+f, self.dest+f, activity=act)
                self.logger.info("Activity transfer %s: Source %s, Destination %s", act, self.source+f, self.dest+f)
                job = fts3.new_job(transfers=[transfer], overwrite=True)
                try:
                    job_id = fts3.submit(self.fts3_context, job)
                    self.job_ids.append(job_id)
                    self.logger.info("Submitted job id %s", job_id)
                except Exception, e:
                    self.logger.error('Failed to submit job: %s' % str(e))
        while True:
            pass
    

        
if __name__ == '__main__':
    import config
    import time
    
    logging.config.fileConfig(config.logpath)
    process_id = int(sys.argv[1])
    activity_test = ActivityShares(config, process_id)
    
    
    
    
    
    
