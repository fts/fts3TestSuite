import datetime
import os


PublicCert = os.environ.get('X509_USER_CERT', '/afs/cern.ch/user/f/ftssuite/.globus/usercert.pem')
PrivateKey = os.environ.get('X509_USER_KEY', '/afs/cern.ch/user/f/ftssuite/.globus/userkey.pem')
CaPath = '/etc/grid-security/certificates'

FTS3Host = os.environ.get('FTS3_HOST', 'fts3-pilot.cern.ch')
FTS3Port = 8446

#logging path
logpath = '/afs/cern.ch/user/f/ftssuite/public/fts3TestSuite/logging.conf'

# Link for activity shares
source='gsiftp://dpmhead-trunk.cern.ch/dpm/cern.ch/home/dteam/'
dest='gsiftp://dpmhead-rc.cern.ch/dpm/cern.ch/home/dteam/'

files=['1.txt', 'AOD.05382619._000103.pool.root.1', 'a28a.root', 'a28b.root', 'aaa', 
       'aaaabbaaa', 'abc12', 'anaconda-ks.cfg', 'asound.conf', 'azzzzz4dsdsds', 'bbb',
        'bbbbd', 'bc12', 'build.out', 'david1-no-reps' ]

# Number of files per activity share
activity_queue = {"Data Consolidation": 20,#779
                  "Express" : 20, #52 
                  "Production Input" : 20,#235 
                  "Production Output" : 20} #6724


