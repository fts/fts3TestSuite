import datetime
import os


PublicCert = os.environ.get('X509_USER_CERT', '/afs/cern.ch/user/f/ftssuite/.globus/usercert.pem')
PrivateKey = os.environ.get('X509_USER_KEY', '/afs/cern.ch/user/f/ftssuite/.globus/userkey.pem')
CaPath = '/etc/grid-security/certificates'

FTS3Host = os.environ.get('FTS3_HOST', 'fts3-pilot.cern.ch')
FTS3Port = 8446

#logging path
logpath = '/afs/cern.ch/user/f/ftssuite/public/fts3TestSuite/logging.conf'

submissions = 1000000
timeout = 3600*3
#submissions=1


source='root://eospublic.cern.ch:1094//eos/opstest/dteam/ftstest/'
dest='root://castorpublic.cern.ch:1094//castor/cern.ch/grid/dteam/ftstests/root/'


