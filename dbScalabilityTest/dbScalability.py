#!/usr/bin/env python
#
# Subscribe to the state change topic, submit to a different FTS3 the same
# job but with mock:// instead
# Can't really get the job, only the transfers. For the moment being, live with it
#

import config
import logging
import logging.config
from datetime import timedelta
import os
import random
import subprocess
import sys
import urllib
import urlparse
import socket
import signal
import sys
import string

import fts3.rest.client.easy as fts3

fts3_host = config.FTS3Host
fts3_port = config.FTS3Port
fts3_capath = config.CaPath
proxy = config.proxy

MaxFiles_1GB=1000
GB = 1073741824
TimeDuration=300
MaxLinks = 4000
MaxTransfers = 5
# Worker
job_finished_status=['FINISHED', 'FAILED', 'FINISHEDDIRTY', 'CANCEL']

 
class dbScalability(object):
    """
    Mirrors requests
    """

    def __init__(self, config, id_process):
        """
        Constructor
        """
        
        self.logger = logging.getLogger('slave')
        self.job_ids = []
        self.register_signals()
        self.logger.error("pid: "+ str(os.getpid()))   
        self.id_process = id_process
        self.submissions = config.submissions
        self.timeout = config.timeout
        

    
    def register_signals(self):
        for i in [x for x in dir(signal) if x.startswith("SIG")]:
            try:
                signum = getattr(signal,i)
                signal.signal(signum, self.signal_handler)
            except Exception, e:
                self.logger.debug("Skipping "+ i)
                  
    def signal_handler(self, signal, frame):
        self.logger.debug("Signal %s", signal)
        fts3_context = fts3.Context(
                        endpoint='https://%s:%d' % (self.host, self.port),
                        capath=self.ca
                        )
        for id in self.job_ids:
            try:
                job_info = fts3.get_job_status(fts3_context, id, list_files=True)
                job_status = 'NONE'
                if job_info != None:
                    job_status = job_info['job_state']
                if job_status not in job_finished_status:
                    fts3.cancel(fts3_context, id)
                self.logger.info("Cancel job %s", id)
            except Exception, e:
                self.logger.error("Failed to cancel job %s with status %s: %s", id, job_status, str(e))
        sys.exit(0)
        

    def run(self):
        start_time = time.time()
        count = 0
	errors = 0
        self.logger.info("Start time %s", str(start_time))
        while (count < self.submissions):
	    maxLinks = 0	
	    if self.id_process in range(1,10):
	    	j = 0
		maxLinks = 1999
	    else:
		j = 2000
		maxLinks = 3999
	    while (j < maxLinks):
	        self.transfers=[]
		for i in range (1, MaxTransfers):
		    f= str(random.randint(1, MaxLinks))
		    name_of_machine = str(socket.gethostname())
		    randomise_dest_surl_uuid = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(4))
                    self.source = config.source+'-'+str(j)+'/data/dteam/'
		    self.dest = config.dest+'-'+str(j)+'/data/dteam/'
            	    fsource = 'more_1GB/testFile_more_1GB'+f+'?checksum=None&size='+str(GB)+'&time='+str(TimeDuration)
            	    fdest = 'more_1GB/testFile_more_1GB'+f+':'+str(j)+':'+str(count)+':'+str(maxLinks)+':'+name_of_machine+':'+randomise_dest_surl_uuid+'?checksum=None&size_post='+str(GB)+'&time='+str(TimeDuration)
            	    transfer = fts3.new_transfer(self.source+fsource, self.dest+fdest, checksum=None, filesize=GB)
            	    self.logger.info("Source %s, Destination %s", self.source+fsource, self.dest+fdest)
		    self.transfers.append(transfer)
		
            	job = fts3.new_job(transfers=self.transfers, verify_checksum=False, overwrite=True, timeout=self.timeout)
	    	try:
	            fts3_context = fts3.Context(
                		endpoint='https://%s:%d' % (fts3_host, fts3_port),
                		capath=fts3_capath, ucert=proxy, ukey=proxy)
                    job_id = fts3.submit(fts3_context, job)
                    self.job_ids.append(job_id)
                    self.logger.info("Submitted job id %s, number of jobs sent %s", job_id, str(count))
                    if count >= self.submissions:
		        break
                except Exception, e:
                    self.logger.error('Failed to submit job: %s' % str(e))
		    errors += 1
		j+=1
            count +=1

	self.logger.info("End test time %s, number of submissions errors %s out of %s", str(time.time()), str(errors), str(count))
        
if __name__ == '__main__':
    import config
    import time
    os.system("bash /tmp/ftssuite/gencredentials.sh")    
    logging.config.fileConfig(config.logpath)
    process_id = int(sys.argv[1])
    dbScalability_test = dbScalability(config, process_id)
    dbScalability_test.run()
    fts3_context = fts3.Context(
                        endpoint='https://%s:%d' % (config.FTS3Host, config.FTS3Port),
                        capath=config.CaPath
                        )
    while True:
        fts3.delegate(fts3_context, lifetime=timedelta(hours=48), force=False)
        pass
    
        
    
    
    
    
