import datetime
import os


PublicCert = os.environ.get('X509_USER_CERT', '/tmp/.globus/robotcert.pem')
PrivateKey = os.environ.get('X509_USER_KEY', '/tmp/.globus/robotkey.pem')
CaPath = '/etc/grid-security/certificates'
proxy = '/tmp/ftssuite/dteam/x509up_u86582'
FTS3Host = os.environ.get('FTS3_HOST', 'fts3-devel-next.cern.ch')
FTS3Port = 8446

#logging path
logpath = '/afs/cern.ch/user/f/ftssuite/public/fts3TestSuite/logging.conf'

submissions = 2000000
#submissions = 10

timeout = 3600*3
source='mock://ftssuitetest-source'
dest='mock://ftssuitetest-dest'
#source='mock://ftssuitetest/data/dteam/'
#dest='mock://ftssuitetest-dest/home/dteam/'
#source = 'gsiftp://eospublicftp.cern.ch/eos/ftstest/'
#destinations = ['srm://f-dpm001.grid.sinica.edu.tw:8446/srm/managerv2?SFN=/dpm/grid.sinica.edu.tw/home/dteam/', 
  #              'srm://srm.pic.es:8443/srm/managerv2?SFN=/pnfs/pic.es/data/dteam/',
   #             'srm://srm-cms.jinr-t1.ru:8443/srm/managerv2?SFN=/pnfs/jinr-t1.ru/data/dteam/',
    #            'srm://ccsrm.in2p3.fr:8443/srm/managerv2?SFN=/pnfs/in2p3.fr/data/dteam/',
     #           'srm://gridka-dCache.fzk.de:8443/srm/managerv2?SFN=/pnfs/gridka.de/dteam/',
      #          'srm://srm.grid.sara.nl:8443/srm/managerv2?SFN=/pnfs/grid.sara.nl/data/dteam/',
       #         's3://ftstests.cs3.cern.ch/',
        #        'srm://storm02.clumeq.mcgill.ca:8444/srm/managerv2?SFN=/dteam/']
