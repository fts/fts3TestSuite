#Script for creating the VM cluster
#@author Maria Arsuaga-Rios (marsuaga)
#!bin/bash
export OS_PROJECT_NAME="IT FTS3 TestSuite"
KEYPAIR="fts3ops"
CC7_IMAGE="CC7 - x86_64 [2018-12-03]"

for n in {001..020}
do
        ai-bs \
         -g fts3testsuite \
         -i "${CC7_IMAGE}" \
         --nova-flavor m2.medium \
         --landb-mainuser 'fts-devel' \
        --landb-responsible 'fts-devel' \
         --nova-sshkey "${KEYPAIR}" ftstest$n
done

