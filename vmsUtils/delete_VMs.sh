#Script for deleting the whole VM cluster
#@author Maria Arsuaga-Rios (marsuaga)
#!bin/bash
export OS_PROJECT_NAME="IT FTS3 TestSuite"
export OS_USERNAME=$USER
for n in {002..010}
do
	ai-kill ftstest$n 
done
