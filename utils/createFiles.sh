#Script for creating files per size and copy them to eos endpoint
#@author Maria Arsuaga-Rios (marsuaga)
#!bin/bash

MB=1024*1024
GB=1024*1024*1024
file_name=testFile_less_1MB
directory=less_1MB

#more_1GB = 120 files
#less_1MB = 30 filesfile_size=$[ RANDOM % $MB ]
#between = 150file_size=$[ $GB + $[ RANDOM % $GB ]]

for i in {1..30}
do
	file_size=$[ RANDOM % $MB ]
	dd if=/dev/zero of=$file_name$i bs=$file_size count=1
	xrdcp $file_name$i root://eospublic.cern.ch//eos/ftstest/$directory/$file_name$i
	rm $file_name$i
done

file_name=testFile_less_1GB
directory=between_1MB_1GB

for i in {1..150}
do
        file_size=$[ $MB + $[ RANDOM % $GB ]]
        dd if=/dev/zero of=$file_name$i bs=$file_size count=1
        xrdcp $file_name$i root://eospublic.cern.ch//eos/ftstest/$directory/$file_name$i
        rm $file_name$i
done
