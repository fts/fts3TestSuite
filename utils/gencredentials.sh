#Script for generation credentials (cronjob)
#@author Maria Arsuaga-Rios (marsuaga)
#!bin/bash

kinit -R
aklog -d
export X509_CERT_DIR=/etc/grid-security/certificates
export X509_VOMSES=/etc/vomses
export X509_USER_PROXY=/tmp/ftssuite/dteam/x509up_u86582
#fetch-crl -l /etc/grid-security/certificates/
arcproxy -S dteam:/dteam/Role=lcgadmin
arcproxy -I

export X509_USER_PROXY=/tmp/ftssuite/bitface/x509up_u86582
arcproxy -S bitface
arcproxy -I
