import datetime
import os

from endpoint import Endpoint,Type
## Config environment
MsgBrokerHost = 'dashb-mb.cern.ch'
MsgBrokerPort = 61123

PublicCert = os.environ.get('X509_USER_CERT', '/tmp/.globus/robotcert.pem')
PrivateKey = os.environ.get('X509_USER_KEY', '/tmp/.globus/robotkey.pem')

proxies = dict()
proxies['bitface'] = '/tmp/ftssuite/bitface/x509up_u86582'
proxies['dteam'] = '/tmp/ftssuite/dteam/x509up_u86582'

CaPath = '/etc/grid-security/certificates'
FTS3Host = os.environ.get('FTS3_HOST', 'fts3-devel-next.cern.ch')
FTS3Port = 8446

BullyHosts = dict(
    sources=['source.host'],
    destinations=['dest.host']
)
BullySleep = datetime.timedelta(milliseconds=10)

#logging path
logpath = '/afs/cern.ch/user/f/ftssuite/public/fts3TestSuite/logging.conf'
error_percent = 0.3
#error_percent = 0.0
physical_percent = 0.0
endpoints = dict()

endpoints[Type.dpm] = Endpoint(Type.dpm,'srm://f-dpm001.grid.sinica.edu.tw:8446/srm/managerv2?SFN=/dpm/grid.sinica.edu.tw/home/dteam', '3T')
endpoints[Type.dcache] = [Endpoint(Type.dcache,'srm://srm.pic.es:8443/srm/managerv2?SFN=/pnfs/pic.es/data/dteam', '1T'),
                           Endpoint(Type.dcache,'srm://srm-cms.jinr-t1.ru:8443/srm/managerv2?SFN=/pnfs/jinr-t1.ru/data/dteam', '2T'),
                           Endpoint(Type.dcache,'srm://ccsrm.in2p3.fr:8443/srm/managerv2?SFN=/pnfs/in2p3.fr/data/dteam', '2T'),
                           Endpoint(Type.dcache,'srm://gridka-dCache.fzk.de:8443/srm/managerv2?SFN=/pnfs/gridka.de/dteam', '10T')]
endpoints[Type.castor] = Endpoint(Type.castor,'srm://srm.grid.sara.nl:8443/srm/managerv2?SFN=/pnfs/grid.sara.nl/data/dteam', 'undef') #dCacheTape
endpoints[Type.eos] = Endpoint(Type.eos,'root://eospublic.cern.ch//eos/ftstest', '2T')
endpoints[Type.s3] = Endpoint(Type.s3,'s3://ftstests.cs3.cern.ch', 'undef')
endpoints[Type.storm] = Endpoint(Type.storm,'srm://storm02.clumeq.mcgill.ca:8444/srm/managerv2?SFN=/dteam', '2T')
