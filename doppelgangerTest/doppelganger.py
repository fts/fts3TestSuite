#!/usr/bin/env python
#
# Subscribe to the state change topic, submit to a different FTS3 the same
# job but with mock:// instead
# Can't really get the job, only the transfers. For the moment being, live with it
#

import errno
import logging
import logging.config
import logging.handlers
import signal
import os
import random
import subprocess
import sys
import urllib
import urlparse
import socket
import hashlib
import types


import config
from endpoint import Type, Endpoint
import fts3.rest.client.easy as fts3
from lib.amq import ActiveMqListener


MB = 1024 * 1024
GB = MB * 1024
MaxFiles_1MB = 30
MaxFiles_1MB_1GB = 150
MaxFiles_1GB = 120
#MinTransfers = 10000
#MaxTransfers = 100000
MinTransfers = 500
MaxTransfers = 1000
UINT16_MAX = 65535
SecondsToDays = 60*60*24
StuckTransferDays = 3
exit = False
DURATION_TEST=7200

# Worker
fts3_host = config.FTS3Host
fts3_port = config.FTS3Port
fts3_capath = config.CaPath
endpoints = config.endpoints
proxies = config.proxies
physical_percent = config.physical_percent
error_percent = config.error_percent 
job_finished_status=['FINISHED', 'FAILED', 'FINISHEDDIRTY', 'CANCEL']
  
class Doppelganger(object):
    """
    Mirrors requests
    """

    ERRNO_MAPPING = dict()
    for errcode in errno.errorcode:
        category = os.strerror(errcode).replace(' ', '_').upper()
        ERRNO_MAPPING[category] = errcode
    ERRNO_MAPPING['GENERAL ERROR'] = errno.EINVAL
    

    def __init__(self, config, id_process, total_processes):
        """
        Constructor
        """
	self.countDest = 0
        self.logger = logging.getLogger("slave")
        self.job_ids=dict()
        self._register_signals()
        self.channel_stats = dict()
        self.process_id = id_process
	self.total_processes = total_processes        
        self.amq = ActiveMqListener(
            host=config.MsgBrokerHost, port=config.MsgBrokerPort,
            cert=config.PublicCert, key=config.PrivateKey
        )
        self.id_process = id_process
        self._calculateHashRange(total_processes)
        self.amq.add_topic('/topic/transfer.fts_monitoring_state', self.process_transfer)
        try:
	       self.amq.start()
        except Exception, e:
            self.logger.error("Finish threads: %s ", str(e))
            sys.exit(0)

        
        
    def _calculateHashRange(self, total_processes):
            
            # Calculate start and end hash values
            segsize = UINT16_MAX / total_processes
            segmod  = UINT16_MAX % total_processes

            start = segsize * (self.id_process-1);
            end   = segsize * (self.id_process) - 1

            # Last one take over what is left
            if (self.id_process == total_processes - 1):
                end += segmod + 1;

            self.hashstart = start
            self.hashend   = end
        
        
    def _register_signals(self):
        signal.signal(15, self.signal_handler)
        signal.signal(2, self.signal_handler)
        
    def signal_handler(self, signal, frame):
        self.logger.info("Signal %s", signal)
        self.logger.info(self.job_ids)
        fts3_context = fts3.Context(
            endpoint='https://%s:%d' % (fts3_host, fts3_port), capath=fts3_capath)
        for id in self.job_ids:
            try: 
                job_info = fts3.get_job_status(fts3_context, id, list_files=True)
                job_status = 'NONE'
                if job_info != None:
                    job_status = job_info['job_state']
                if job_status not in job_finished_status:
                    fts3.cancel(fts3_context, id)
                self.logger.info("Cancel job %s", id)
            except Exception, e:
                self.logger.error("Failed to cancel job %s with status %s: %s", id, job_status, str(e))
                    
        sys.exit(0) 

    def _get_file_url(self, filesize):
        if filesize <= MB:
            file_path = 'less_1MB/testFile_less_1MB' + str(random.randint(1, MaxFiles_1MB))
        else:
            if (filesize > MB) & (filesize <= GB):
                file_path = 'between_1MB_1GB/testFile_less_1GB' + str(random.randint(1, MaxFiles_1MB_1GB))
            else:
                file_path = 'more_1GB/testFile_more_1GB' + str(random.randint(1, MaxFiles_1GB))
        return file_path

    def _rewrite_url(self, url, se_url, checksum, filesize, file_path, is_dest, errcode, transfer_errcode, throughput=None, isMock=True):
        """
        Rewrite to a mock url
        """
        
        parsed = urlparse.urlparse(url)
        query = urlparse.parse_qs(parsed.query)
        if parsed.scheme == 'srm' and 'SFN' in query:
            path = query['SFN'][0]
            del query['SFN']
        else:
            path = parsed.path        

        if not filesize:
            filesize = 1024 * 1024
                   
        if is_dest:
            query['size_post'] = filesize
        else:
            query['size'] = filesize
        
        
        # Emulate duration with the stored throughput, or random otherwise
        if throughput is None:
            throughput = random.randint(1024 * 1024, 50 * 1024 * 1024)
        duration = int(filesize / throughput)
        if duration == 0:
            duration = 3
        query['time'] = duration

        #if errcode:
         #   query['errno'] = errcode
        #if transfer_errcode:
         #   query['transfer_errno'] = transfer_errcode

        if random.random() <  error_percent:
	   #query['errno'] = 2
           query['transfer_errno'] = 70
	else:
	   if 'errno' in query:
	   	del query['errno']
	   if 'transfer_errno' in query:
	   	del query['transfer_errno']
	   

	#se_url = self._get_mapped_se_url(se)
       
        if str(se_url).startswith('s3'):
            checksum = None
        query['checksum'] = checksum
        
	
        if isMock:
            surl = urlparse.urlunparse(('mock', parsed.hostname, path, None, urllib.urlencode(query), parsed.fragment))
            self.logger.debug("Se Mock url: %s" % surl)
        else: 
            protocol = str(se_url).split(':')
            surl = str(se_url) + '/' + str(file_path)
            self.logger.debug("Se physical url: %s" % surl)
        return surl
        # return urlparse.urlunparse(('mock', parsed.hostname, path, None, urllib.urlencode(query), parsed.fragment))

    @staticmethod
    def _probabilistic_error(stats):
        """
        From a stat dictionary, throw a coin and decide if an error
        takes place following the distribution seen in the 'real-life'
        Return source_errno, destination_errno, transfer_errno
        """
        if stats is None:
            return None, None, None

        total = stats.get('total', 0)
        if not total:
            return None, None, None

        dice = random.random() * total
        accumulate = 0
        for key, value in stats.iteritems():
            if not key.startswith('error') and key != 'ok':
                continue
            start = accumulate
            accumulate += value
            if dice >= start and dice < accumulate:
                if key == 'ok':
                    return None, None, None

                _, phase, errcode = key.split('||')
                if phase == 'SOURCE':
                    return int(errcode), None, None
                elif phase == 'TRANSFER':
                    return None, None, int(errcode)
                else:
                    return None, int(errcode), None

        self.logger.error("%d %.2f %d" % (total, dice, accumulate))
        raise AssertionError('Should not be reached')
    
   
    def _get_mapped_se_url(self, se, gen_random=False):
        """
        Return the mapped se url from the se available in the test set.
        """        
        query = 'ldapsearch -LLL -x -H ldap://lcg-bdii.cern.ch:2170 -b o=grid "(&(objectClass=GlueSE)(GlueSEUniqueID=' + se + '))" GlueSEImplementationName | grep GlueSEImplementationName: | cut -c27-'
        proc = subprocess.Popen(query, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        se_type, unused_err = proc.communicate()
       
        if not se_type:
            se_type = 'undefined'
        se_type = se_type.split()[0]
        se_type = se_type.lower()
        random_type = se_type
        if gen_random:
            while random_type == se_type:
                random_type, endpoint = random.choice(list(endpoints.items()))
          
            self.logger.debug("Random endpoint %s random type %s" % (endpoint, random_type))
            if isinstance(endpoint, list):
                endpoint = random.choice(endpoint)
            self.logger.debug("Random endpoint %s random type %s" % (endpoint, random_type))
            return endpoint.get_surl()
            
        self.logger.debug("Se type: %s" % se_type)
        
        if se_type in endpoints.keys():
            if se_type != Type.dcache:
                endpoint = endpoints.get(se_type)
                return endpoint.get_surl()
            else:
                endpoint = random.choice(endpoints.get(se_type))
                return endpoint.get_surl()
        else:
            if (se_type == 'bestman') | (se_type == 'castor'):
                endpoint = endpoints.get('eos')
                return endpoint.get_surl()
            if (se_type == 'hdfs') | (se_type == 'undefined'):
                endpoint = endpoints.get('s3')
                return endpoint.get_surl()
            return
        
       
    def _generate_extra_sources(self, source, checksum, filesize):
        """
        Return a list of alternative sources picking some random known storages
        """
        known_triplets = self.channel_stats.keys()
        if len(known_triplets) == 0:
            return list()

        parsed = urlparse.urlparse(source)
        alternatives = list()
        for i in range(random.randint(1, 5)):
            key = random.choice(known_triplets)
            source_se = urlparse.urlparse(key[1]).hostname

            stats = self.channel_stats.get(key, None)
            source_err, _, _ = Doppelganger._probabilistic_error(stats)

            source_with_new_host = urlparse.urlunparse(
                ('mock', source_se, parsed.path, None, None, None)
            )

            new_source_url = self._rewrite_url(
                source_with_new_host, checksum, filesize, False,
                None, None, stats.get('throughput', None)
            )
            if new_source_url:
                alternatives.append(new_source_url)

        return alternatives

   
   
    def check_status(self, job_id, fts3_context): 
        self.logger.info('Checking the status')
        start_time = time.time()
        status = 'NONE'
        while status not in job_finished_status:
            try:
                duration_time = (time.time() - start_time)/SecondsToDays
                if duration_time > StuckTransferDays:
                    self.logger.error('Stuck job %s', job_id)
                    return
                job_info = fts3.get_job_status(fts3_context, job_id, list_files=True)
                if job_info != None:
                    job_status = job_info['job_state']
                    if (job_status == 'FINISHED') | (job_status == 'FAILED') | (job_status == 'FINISHEDDIRTY'):
                        self.logger.info('Job status: %s', job_status)
                        finished = 0
                        failed = 0
                        cancel = 0
                        for file in job_info['files']:
                            state = file['file_state']
                            self.logger.info('File id %s status %s', file['file_id'], state)
                            if state == 'FINISHED':
                                finished += 1
                            if state == 'FAILED':
                                failed += 1
                            if state == 'CANCEL':
                                cancel +=1
                        
                        total_files = cancel + finished + failed
                        if (job_status == 'FINISHED') & ( finished != total_files):
                            self.logger.error('Fatal inconsistent status in job %s, job status %s, finished number %i, total %i', job_id, job_status, finished, total_files)
                        if (job_status == 'FAILED') & (failed != total_files):
                            self.logger.error('Fatal inconsistent status in job %s, job status %s, finished number %i, total %i', job_id, job_status, finished, total_files)
                        if (job_status == 'FINISHEDDIRTY') & ((failed == total_files) | (finished == total_files)):
                            self.logger.error('Fatal inconsistent status in job %s, job status %s, finished number %i, total %i', job_id, job_status, finished, total_files)
                         
                        return
                else:
                    self.logger.debug ("Job_info is None")
                time.sleep(60)
            except Exception, e:
                self.logger.error('Not state recovery %s', str(e))
                return
            
    def process_transfer(self, message):
        """
        Submit a transfer with the intercepted urls (except using mock)
        """
        
        hashed_id = random.randint(0, (2 ** 16) - 1)
        
       # if (hashed_id < self.hashstart) | (hashed_id > self.hashend):
        #    self.logger.info("Not in hash: Thread finished")
        #    return
        state = message['file_state']
        if state != 'SUBMITTED':
            self.logger.info("Not submitted: Thread finished")
            return
        
        self.amq.disconnect('/topic/transfer.fts_monitoring_state')
        self.logger.debug("Process %s: Hash job_id %s in range start %s - end %s",str(self.id_process), str(hashed_id), str(self.hashstart), str(self.hashend))
       
        vo = message['vo_name']
        source = message['src_url']
        dest = message['dst_url']
        metadata = message['file_metadata']
        job_metadata = message['job_metadata']
        # Make sure we do not retro-feed ourselves!
        if source.startswith('mock'):
            self.logger.info("Retrofeed:Thread finished")
            return
     
         # Only makes sense for Atlas submissions really
        if isinstance(metadata, dict):
            checksum = metadata.get('adler32', None)
            filesize = metadata.get('filesize', None)
            activity = metadata.get('activity', None)
        else:
            checksum = None
            filesize = None
            activity = None
     
        if isinstance(job_metadata, dict):
            is_multisource = job_metadata.get('multi_sources', False)
        else:
            is_multisource = False
     
        # Use stored statistics to generate urls that will fail
        
        (files, verify) =self._generateTransfers(vo, source, dest, filesize, activity)
        
        job = fts3.new_job(transfers=files, verify_checksum=verify, overwrite=True)
        vo, proxy = random.choice(list(proxies.items()))
        try:
            fts3_context = fts3.Context(
            endpoint='https://%s:%d' % (fts3_host, fts3_port),
            capath=fts3_capath, ucert=proxy, ukey=proxy)
            job_id = fts3.submit(fts3_context, job)
            self.job_ids[job_id] = files
            self.logger.info(" Job id %s, Process id %s, VO %s", job_id, self.id_process, vo)
            self.check_status(job_id, fts3_context)
            #self.status_thread = threading.Thread(target=self.check_status, args=(job_id))
            ##self.status_thread.join()
               
            self.logger.info("Thread finished")
        except Exception, e:
            self.logger.error('Failed to submit job: %s' % str(e))
        return
    
            #if is_multisource:
                #self.logger.info('%s (multisource) %s => %s' % (job_id, source, dest))
            #else:
               # self.logger.info('%s %s => %s' % (job_id, source, dest))
            #self.check_job_status(job_id)
    
    def _generateTransfers(self, vo, source, dest, filesize, activity):
        
        transfers=[]
        file_ids=[]
        verify = True
        checksum = None
        source_se = Endpoint._get_se(source)
	self.countDest = self.countDest + 1
        dest_se = Endpoint._get_se(dest)+ str(self.countDest)
        self.logger.info("Original source se %s, dest se %s", source_se, dest_se)
        key = (vo, source_se, dest_se)
        num_transfers = random.randint(MinTransfers, MaxTransfers)
        self.logger.info("Number of transfers to generate %i", num_transfers)
        source_url = self._get_mapped_se_url(source_se)
        dest_url = self._get_mapped_se_url(dest_se)
        if source_url == dest_url:
            dest_url = self._get_mapped_se_url(dest_se, gen_random=True)
        isMock = self._isMock()
        for i in range(1, num_transfers):
            stats = self.channel_stats.get(key, None)
            source_err, dest_err, transfer_err = Doppelganger._probabilistic_error(stats)
            if stats:
                throughput = stats.get('throughput', None)
            else:
                throughput = None
            file_path = self._get_file_url(filesize)
            # Rewrite urls 
            source = self._rewrite_url(source, source_url, checksum, filesize, file_path, False, source_err, None, isMock=isMock)
            dest = self._rewrite_url(dest, dest_url, checksum, filesize, file_path, True, dest_err, transfer_err, throughput, isMock=isMock)
            current_verify = self._supportChecksum(source, dest)
            if current_verify == False:
                verify = False
            
            transfer = fts3.new_transfer(source, dest, activity=activity, checksum=checksum)
            
            # If it is a multisource, generate a random number of alternatives
            # and push them
            #if is_multisource:
                #transfer['sources'].extend(self._generate_extra_sources(source, checksum, filesize))
                #transfer['selection_strategy'] = 'auto'
            
            transfers.append(transfer)
        return (transfers, verify)
            #job = fts3.new_job(transfers=[transfer], verify_checksum=(checksum is not None), overwrite=True)
            #s3 does not support verify_checksum
            
    def _isMock(self):
        if random.random() > physical_percent:
            return True
        else:
            return False
    
    def _supportChecksum(self, source, dest):
        if source.startswith('s3'):
            return False
        if dest.startswith('s3'):
            return False
        return True

    def _map_to_errno(self, category):
        """
        Return the error code corresponding to the error category
        """
        if category in Doppelganger.ERRNO_MAPPING:
            return Doppelganger.ERRNO_MAPPING[category]
        else:
            self.logger.warning('Unknown category: %s' % category)
            return errno.ECONNREFUSED

        
 

if __name__ == '__main__':
    import config
    import time
    process_id = int(sys.argv[1])
    total_processes = int(sys.argv[2])
    logging.config.fileConfig(config.logpath)
    
    exit = False
    doppelganger = Doppelganger(config, process_id, total_processes)
    while not exit:
       time.sleep(DURATION_TEST)
       exit = True
    if exit:
        sys.exit(0)
