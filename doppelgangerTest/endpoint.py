#!/usr/bin/env python

import subprocess
import time
import urlparse


class Type:
    dcache ="dcache"
    storm = "storm"
    dpm = "dpm"
    eos = "eos"
    s3 = "s3"
    castor = "castor"

class Endpoint:
    """ Test endpoint"""
    
    def __init__(self, type, surl, size):
        self.type = type
        self.surl = surl
        self.size = size
        self.space = size
        self.se = Endpoint._get_se(self.surl)
        query = 'ldapsearch -x -LLL -h top-bdii:2170 -b o=grid "(&(objectClass=GlueSE)(GlueSEUniqueID='+self.se+'))" GlueSEImplementationVersion | grep GlueSEImplementationVersion: | cut -c30- | head -1'
        proc = subprocess.Popen(query, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        version, unused_err = proc.communicate()
        if not version:
            self.version='undefined'
        self.version = version.split()
        self.time_version = time.time()
        
    @staticmethod
    def _get_se(url):
        """
        Extract the storage from the url
        """
        parsed = urlparse.urlparse(url)
        return '%s' %(parsed.hostname)
    
    def _update_space(self, file_size):
        self.space = self.space - file_size
        
    
    def get_surl(self):
        return self.surl
    
    def get_space(self):
        return self.space
    
    def get_size(self):
        return self.size
    
    def check_space (self, file_size):
        if self.space - file_size <= 0:
            return False
        else:
            self._update_space(file_size)
            return True
    
    
    def set_type_version(self):
        query = 'ldapsearch -x -LLL -h top-bdii:2170  -b o=grid "(&(objectClass=GlueSE)(GlueSEUniqueID='+self.se+'))" GlueSEImplementationVersion | grep GlueSEImplementationVersion: | cut -c30-'
        version = subprocess.check_output(query, shell=True)
        if version != self.version:
            self.version = version
            self.time_version = time.time()

    
        
