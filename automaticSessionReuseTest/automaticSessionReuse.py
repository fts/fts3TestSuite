#!/usr/bin/env python
#
# Subscribe to the state change topic, submit to a different FTS3 the same
# job but with mock:// instead
# Can't really get the job, only the transfers. For the moment being, live with it
#


import logging
import logging.config
from datetime import timedelta
import os
import random
import subprocess
import sys
import urllib
import urlparse
import socket
import signal
import sys

import fts3.rest.client.easy as fts3

#This test will rull 3 submissions
#1. 2k files in one job without filesize -> no session reuse
#2. 1K files in one job with filesize 1MB -> no session reuse
#3. less 1k files in one job with filesize with 1 file with 1GB -> session reuse
#4. less 1k files without filesize -> no session reuse
#5. less 1k files with filesize but with 3 files GB -> no session reuse

MaxFiles1K = 1000
MaxFiles2K = 2000
MB = 1024 * 1024
GB = MB * 1024

MaxFiles_1MB = 30
MaxFiles_1MB_1GB = 150
MaxFiles_1GB = 1000

TimeDuration = 1200

# Worker
job_finished_status = ['FINISHED', 'FAILED', 'FINISHEDDIRTY', 'CANCELED']

 
class AutomaticSessionReuse(object):
    """
    Mirrors requests
    """

    def __init__(self, config, id_process):
        """
        Constructor
        """
        
        self.logger = logging.getLogger('slave')
        self.job_ids = []
        self.register_signals()
        self.logger.error("pid: "+ str(os.getpid()))   
        self.id_process = id_process
        self.host = config.FTS3Host
        self.port = config.FTS3Port
        self.ca = config.CaPath
        self.source = config.sources
        self.dest = config.dests
        self.timeout = config.timeout
        self.proxy = config.proxy        

    
    def register_signals(self):
        for i in [x for x in dir(signal) if x.startswith("SIG")]:
            try:
                signum = getattr(signal,i)
                signal.signal(signum, self.signal_handler)
            except Exception, e:
                self.logger.debug("Skipping "+ i)
                  
    def signal_handler(self, signal, frame):
        self.logger.debug("Signal %s", signal)
        fts3_context = fts3.Context(
                        endpoint='https://%s:%d' % (self.host, self.port),
                        capath=self.ca, ucert=self.proxy, ukey=self.proxy)
        for id in self.job_ids:
            try:
                job_info = fts3.get_job_status(fts3_context, id, list_files=True)
                job_status = 'NONE'
                if job_info != None:
                    job_status = job_info['job_state']
                if job_status not in job_finished_status:
                    fts3.cancel(fts3_context, id)
                self.logger.info("Cancel job %s", id)
            except Exception, e:
                self.logger.error("Failed to cancel job %s with status %s: %s", id, job_status, str(e))
        sys.exit(0)
        

    def _get_file_url(self, filesize):
        if filesize <= MB:
            file_path = 'less_1MB/testFile_less_1MB' + str(random.randint(1, MaxFiles_1MB))
        else:
            if (filesize > MB) & (filesize <= GB):
                file_path = 'between_1MB_1GB/testFile_less_1GB' + str(random.randint(1, MaxFiles_1MB_1GB))
            else:
                file_path = 'more_1GB/testFile_more_1GB' + str(random.randint(1, MaxFiles_1GB))
        return file_path

    def run(self):
        start_time = time.time()
        count = 0
        self.logger.info("Start time %s", str(start_time))
        
        #1. 2k files in one job without filesize -> no session reuse
        files= []
        for i in range (1, MaxFiles2K):
            fsource = '/nosize/testFile_1MB_' + str(i)+'&size='+str(MB)
            fdest = '/nosize/testFile_1MB_' + str(i)+'&size_post='+str(MB)+'&time='+str(TimeDuration)
            transfer = fts3.new_transfer(self.source[0]+fsource, self.dest[0]+'1'+fdest, checksum=None)
            files.append(transfer)
        self.logger.info("Source %s, Destination %s", self.source[0]+fsource, self.dest[0]+fdest)
        job = fts3.new_job(transfers=files, verify_checksum=False, overwrite=True, timeout=self.timeout)
        try:
            fts3_context = fts3.Context(
            endpoint='https://%s:%d' % (self.host, self.port),
                        capath=self.ca, ucert=self.proxy, ukey=self.proxy)
            
            
            job_id = fts3.submit(fts3_context, job)
            if job_id is not None:
                count +=1
                self.job_ids.append(job_id)
                self.logger.info("Submitted job id %s, number of jobs sent %s", str(job_id), str(count))
                        
        except Exception, e:
            self.logger.error('Failed to submit job: %s' % str(e))
        
        #2. 1K files in one job with filesize 1MB -> no session reuse
        transfers= []
        for i in range (1, MaxFiles1K):
            fsource = '/size/1MB' + str(i)+'&size='+str(MB)
            fdest = '/size/1MB' + str(i)+'&size_post='+str(MB)+'&time='+str(TimeDuration)
            transfer = fts3.new_transfer(self.source[1]+fsource, self.dest[1]+fdest, checksum=None, filesize=MB)
            transfers.append(transfer)
        self.logger.info("Source %s, Destination %s", self.source[1]+fsource, self.dest[1]+fdest)
        job = fts3.new_job(transfers=transfers, verify_checksum=False, overwrite=True, timeout=self.timeout)
        try:
            fts3_context = fts3.Context(
            endpoint='https://%s:%d' % (self.host, self.port),
                        capath=self.ca, ucert=self.proxy, ukey=self.proxy)
            
            job_id = fts3.submit(fts3_context, job)
            if job_id is not None:
                count +=1
                self.job_ids.append(job_id)
                self.logger.info("Submitted job id %s, number of jobs sent %s", str(job_id), str(count))
                         
        except Exception, e:
            self.logger.error('Failed to submit job: %s' % str(e))
         
        #3. less 1k files in one job with filesize with 1 file with 1GB -> session reuse
        transfers= []
        for i in range (1, MaxFiles1K-4):
            fsource = '/size/1MB' + str(i)+'&size='+str(MB)
            fdest = '/size/1MB' + str(i)+'&size_post='+str(MB)+'&time='+str(TimeDuration)
            transfer = fts3.new_transfer(self.source[2]+fsource, self.dest[2]+fdest, checksum=None, filesize=MB)
            transfers.append(transfer)
        self.logger.info("Source %s, Destination %s", self.source[2]+fsource, self.dest[2]+fdest)
        fsource = '/size/1GB' + str(i)+'&size='+str(GB)
        fdest = '/size/1GB' + str(i)+'&size_post='+str(GB)+'&time='+str(TimeDuration)
        transfer = fts3.new_transfer(self.source[2]+fsource, self.dest[2]+fdest, checksum=None, filesize=GB)
        transfers.append(transfer)
        job = fts3.new_job(transfers=transfers, verify_checksum=False, overwrite=True, timeout=self.timeout)
        try:
            fts3_context = fts3.Context(
            endpoint='https://%s:%d' % (self.host, self.port),
                        capath=self.ca, ucert=self.proxy, ukey=self.proxy)
           
            job_id = fts3.submit(fts3_context, job)
            if job_id is not None:
                count +=1
                self.job_ids.append(job_id)
                self.logger.info("Submitted job id %s, number of jobs sent %s", str(job_id), str(count))
                         
        except Exception, e:
            self.logger.error('Failed to submit job: %s' % str(e))
             
        #4. less 1k files without filesize -> no session reuse
        transfers= []
        for i in range (1, MaxFiles1K-4):
            fsource = '/nosize/1MB' + str(i)+'&size='+str(MB)
            fdest = '/nosize/1MB' + str(i)+'&size_post='+str(MB)+'&time='+str(TimeDuration)
            transfer = fts3.new_transfer(self.source[3]+fsource, self.dest[3]+fdest, checksum=None)
            transfers.append(transfer)
        self.logger.info("Source %s, Destination %s", self.source[3]+fsource, self.dest[3]+fdest)
        job = fts3.new_job(transfers=transfers, verify_checksum=False, overwrite=True, timeout=self.timeout)
        try:
            fts3_context = fts3.Context(
            endpoint='https://%s:%d' % (self.host, self.port),
                        capath=self.ca, ucert=self.proxy, ukey=self.proxy)
           
            job_id = fts3.submit(fts3_context, job)
            if job_id is not None:
                count +=1
                self.job_ids.append(job_id)
                self.logger.info("Submitted job id %s, number of jobs sent %s", str(job_id), str(count))
                         
        except Exception, e:
            self.logger.error('Failed to submit job: %s' % str(e))
         
         
        #5. less 1k files with filesize but with 2 files GB -> no session reuse
        transfers= []
        for i in range (1, MaxFiles1K-6):
            fsource = '/size/1MB' + str(i)+'&size='+str(MB)
            fdest = '/size/1MB' + str(i)+'&size_post='+str(MB)+'&time='+str(TimeDuration)
            transfer = fts3.new_transfer(self.source[4]+fsource, self.dest[4]+fdest, checksum=None, filesize=MB)
            transfers.append(transfer)
            self.logger.info("Source %s, Destination %s", self.source[4]+fsource, self.dest[4]+fdest)
        fsource = '/size/1GB' + str(i)+'&size='+str(GB)
        fdest = '/size/1GB' + str(i)+'&size_post='+str(GB)+'&time='+str(TimeDuration)
        transfer = fts3.new_transfer(self.source[4]+fsource, self.dest[4]+fdest, checksum=None, filesize=GB)
        transfers.append(transfer)
        fsource = '/size/1GB' + str(i)+'&size='+str(GB)
        fdest = '/size/1GB' + str(i)+'&size_post='+str(GB)+'&time='+str(TimeDuration)
        transfer = fts3.new_transfer(self.source[4]+fsource, self.dest[4]+fdest, checksum=None, filesize=GB)
        transfers.append(transfer)
        job = fts3.new_job(transfers=transfers, verify_checksum=False, overwrite=True, timeout=self.timeout)
 
        try:
            fts3_context = fts3.Context(
            endpoint='https://%s:%d' % (self.host, self.port),
                        capath=self.ca, ucert=self.proxy, ukey=self.proxy)
           
            job_id = fts3.submit(fts3_context, job)
            if job_id is not None:
                count +=1
                self.job_ids.append(job_id)
                self.logger.info("Submitted job id %s, number of jobs sent %s", str(job_id), str(count))
                         
        except Exception, e:
            self.logger.error('Failed to submit job: %s' % str(e))
        self.logger.info("End test time %s", str(time.time()))
        
if __name__ == '__main__':
    import config
    import time
    
    logging.config.fileConfig(config.logpath)
    process_id = int(sys.argv[1])
    #process_id = 1
    automatic_session_reuse_test = AutomaticSessionReuse(config, process_id)
    automatic_session_reuse_test.run()
    
    while True:
        time.sleep(300)
        fts3_context = fts3.Context(
                        endpoint='https://%s:%d' % (config.FTS3Host, config.FTS3Port),
                        capath=config.CaPath, ucert=config.proxy, ukey=config.proxy)
        fts3.delegate(fts3_context, lifetime=timedelta(hours=48), force=False)
        pass
    
        
    
    
    
    
