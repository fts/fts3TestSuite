import datetime
import os


PublicCert = os.environ.get('X509_USER_CERT', '/afs/cern.ch/user/f/ftssuite/.globus/robotcert.pem')
PrivateKey = os.environ.get('X509_USER_KEY', '/afs/cern.ch/user/f/ftssuite/.globus/robotkey.pem')
CaPath = '/etc/grid-security/certificates'
#proxy = '/tmp/ftssuite/dteam/x509up_u86582'
proxy = '/tmp/ftssuite/bitface/x509up_u86582'
FTS3Host = os.environ.get('FTS3_HOST', 'fts3-devel.cern.ch')
FTS3Port = 8446

#logging path
logpath = '/afs/cern.ch/user/f/ftssuite/public/fts3TestSuite/logging.conf'


timeout = 3600*3

sources = ['mock://eospublicftp1.cern.ch/eos/ftstest/', 'mock://eospublicftp2.cern.ch.cern.ch/eos/ftstest/', 
           'mock://eospublicftp3.cern.ch.cern.ch/eos/ftstest/','mock://eospublicftp4.cern.ch.cern.ch/eos/ftstest/',
           'mock://eospublicftp5.cern.ch.cern.ch/eos/ftstest/']
dests = ['mock://srm.grid.sara.nl1/ftstest/', 'mock://srm.grid.sara.nl2/ftstest/', 'mock://srm.grid.sara.nl3/ftstest/',
         'mock://srm.grid.sara.nl4/ftstest/','mock://srm.grid.sara.nl5/ftstest/']











