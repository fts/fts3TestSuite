#!/usr/bin/env python
#
# Subscribe to the state change topic, submit to a different FTS3 the same
# job but with mock:// instead
# Can't really get the job, only the transfers. For the moment being, live with it
#


import logging
import logging.config
from datetime import timedelta
import os
import random
import subprocess
import sys
import urllib
import urlparse
import socket
import signal
import sys
import itertools
import fts3.rest.client.easy as fts3


MB = 1024 * 1024
GB = MB * 1024
MaxFiles_1MB = 30
MaxFiles_1MB_1GB = 150
#MaxFiles_1GB = 120
MaxFiles_1GB=1000
Transfers = 10


# Worker
job_finished_status=['FINISHED', 'FAILED', 'FINISHEDDIRTY', 'CANCEL']

 
class Optimizer(object):
    """
    Mirrors requests
    """

    def __init__(self, config, id_process):
        """
        Constructor
        """
        
        self.logger = logging.getLogger('slave')
        self.job_ids = []
        self.register_signals()
        self.logger.error("pid: "+ str(os.getpid()))   
        self.id_process = id_process
        self.host = config.FTS3Host
        self.port = config.FTS3Port
        self.ca = config.CaPath
        self.source = config.source
        #self.sources = config.sources
        self.dest = config.dest
        self.submissions = config.submissions
        self.timeout = config.timeout
        

    
    def register_signals(self):
        for i in [x for x in dir(signal) if x.startswith("SIG")]:
            try:
                signum = getattr(signal,i)
                signal.signal(signum, self.signal_handler)
            except Exception, e:
                self.logger.debug("Skipping "+ i)
                  
    def signal_handler(self, signal, frame):
        self.logger.debug("Signal %s", signal)
        fts3_context = fts3.Context(
                        endpoint='https://%s:%d' % (self.host, self.port),
                        capath=self.ca
                        )
        for id in self.job_ids:
            try:
                job_info = fts3.get_job_status(fts3_context, id, list_files=True)
                job_status = 'NONE'
                if job_info != None:
                    job_status = job_info['job_state']
                if job_status not in job_finished_status:
                    fts3.cancel(fts3_context, id)
                self.logger.info("Cancel job %s", id)
            except Exception, e:
                self.logger.error("Failed to cancel job %s with status %s: %s", id, job_status, str(e))
        sys.exit(0)
        

    def _get_file_url(self, filesize):
        if filesize <= MB:
            file_path = 'less_1MB/testFile_less_1MB' + str(random.randint(1, MaxFiles_1MB))
        else:
            if (filesize > MB) & (filesize <= GB):
                file_path = 'between_1MB_1GB/testFile_less_1GB' + str(random.randint(1, MaxFiles_1MB_1GB))
            else:
                file_path = 'more_1GB/testFile_more_1GB' + str(random.randint(1, MaxFiles_1GB))
        return file_path

    def run(self):
        start_time = time.time()
        count = 0
        self.logger.info("Start time %s", str(start_time))
        while count < self.submissions:
            #fsource = "more_1GB/testFile_more_1GB14"
	    #for i, j in zip(range(1, 31), self.sources):
            for i in range(1, MaxFiles_1GB):        
                #fdest = 'test_tsystem/testFile_more_1GB'+str(random.randint(1, 1000))
                f = 'more_1GB/testFile_more_1GB'+str(i)
                #transfer = fts3.new_transfer(self.source+fsource, self.dest+f, checksum=None)
                transfer = fts3.new_transfer(self.source+f, self.dest+f, checksum=None)
                self.logger.info("Source %s, Destination %s", self.source+f, self.dest+f)
                job = fts3.new_job(transfers=[transfer], verify_checksum=False, overwrite=True, timeout=self.timeout)
                try:
                    fts3_context = fts3.Context(
                        endpoint='https://%s:%d' % (self.host, self.port),
                        capath=self.ca
                        )
                    
                    job_id = fts3.submit(fts3_context, job)
                    count +=1
                    self.job_ids.append(job_id)
                    self.logger.info("Submitted job id %s, number of jobs sent %s", job_id, str(count))
                    if count >= self.submissions:
                        break
                except Exception, e:
                    self.logger.error('Failed to submit job: %s' % str(e))
        self.logger.info("End test time %s", str(time.time()))
        
if __name__ == '__main__':
    import config
    import time
    
    logging.config.fileConfig(config.logpath)
    process_id = int(sys.argv[1])
    
    optimizer_test = Optimizer(config, process_id)
    optimizer_test.run()
    
    while True:
        time.sleep(300)
        fts3_context = fts3.Context(
                        endpoint='https://%s:%d' % (config.FTS3Host, config.FTS3Port),
                        capath=config.CaPath
                        )
        fts3.delegate(fts3_context, lifetime=timedelta(hours=48), force=False)
        pass
    
        
    
    
    
    
