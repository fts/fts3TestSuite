import datetime
import os


PublicCert = os.environ.get('X509_USER_CERT', '/afs/cern.ch/user/f/ftssuite/.globus/usercert.pem')
PrivateKey = os.environ.get('X509_USER_KEY', '/afs/cern.ch/user/f/ftssuite/.globus/userkey.pem')
CaPath = '/etc/grid-security/certificates'

FTS3Host = os.environ.get('FTS3_HOST', 'fts3-devel.cern.ch')
FTS3Port = 8446

#logging path
logpath = '/afs/cern.ch/user/f/ftssuite/public/fts3TestSuite/logging.conf'

submissions = 1000000
timeout = 3600*3



source='root://eospublic.cern.ch//eos/workspace/f/fts/test/'
dest='srm://f-dpm001.grid.sinica.edu.tw:8446/srm/managerv2?SFN=/dpm/grid.sinica.edu.tw/home/dteam/'
#source='srm://ccsrm.in2p3.fr:8443/srm/managerv2?SFN=/pnfs/in2p3.fr/data/dteam/'
#dest='srm://srm.grid.sara.nl:8443/srm/managerv2?SFN=/pnfs/grid.sara.nl/data/dteam/'
#dest='gsiftp://xdpmhn01.tsy.cern.ch/dpm/tsy.cern.ch/home/dteam/'
#dest='srm://xdpmhn01.tsy.cern.ch:8446/srm/managerv2?SFN=/dpm/tsy.cern.ch/home/dteam/'
#dest = 'root://eospublic.cern.ch//eos/ftstest/test_tsystem'

# sources = ['root://xdpmhn01.tsy.cern.ch/dpm/tsy.cern.ch/home/dteam/test_andrea/testFile_more_1GB138',
#            'root://xdpmhn01.tsy.cern.ch/dpm/tsy.cern.ch/home/dteam/test_andrea/testFile_more_1GB139',
#            'root://xdpmhn01.tsy.cern.ch/dpm/tsy.cern.ch/home/dteam/test_andrea/testFile_more_1GB140',
#            'root://xdpmhn01.tsy.cern.ch/dpm/tsy.cern.ch/home/dteam/test_andrea/testFile_more_1GB141',
#            'root://xdpmhn01.tsy.cern.ch/dpm/tsy.cern.ch/home/dteam/test_andrea/testFile_more_1GB143',
#            'root://xdpmhn01.tsy.cern.ch/dpm/tsy.cern.ch/home/dteam/test_andrea/testFile_more_1GB144',
#            'root://xdpmhn01.tsy.cern.ch/dpm/tsy.cern.ch/home/dteam/test_andrea/testFile_more_1GB147',
#            'root://xdpmhn01.tsy.cern.ch/dpm/tsy.cern.ch/home/dteam/test_andrea/testFile_more_1GB148',
#            'root://xdpmhn01.tsy.cern.ch/dpm/tsy.cern.ch/home/dteam/test_andrea/testFile_more_1GB15',
#            'root://xdpmhn01.tsy.cern.ch/dpm/tsy.cern.ch/home/dteam/test_andrea/testFile_more_1GB153',
#            'root://xdpmhn01.tsy.cern.ch/dpm/tsy.cern.ch/home/dteam/test_andrea/testFile_more_1GB154',
#            'root://xdpmhn01.tsy.cern.ch/dpm/tsy.cern.ch/home/dteam/test_andrea/testFile_more_1GB157',
#            'root://xdpmhn01.tsy.cern.ch/dpm/tsy.cern.ch/home/dteam/test_andrea/testFile_more_1GB158',
#            'root://xdpmhn01.tsy.cern.ch/dpm/tsy.cern.ch/home/dteam/test_andrea/testFile_more_1GB159',
#            'root://xdpmhn01.tsy.cern.ch/dpm/tsy.cern.ch/home/dteam/test_andrea/testFile_more_1GB160',
#            'root://xdpmhn01.tsy.cern.ch/dpm/tsy.cern.ch/home/dteam/test_andrea/testFile_more_1GB161',
#            'root://xdpmhn01.tsy.cern.ch/dpm/tsy.cern.ch/home/dteam/test_andrea/testFile_more_1GB162',
#            'root://xdpmhn01.tsy.cern.ch/dpm/tsy.cern.ch/home/dteam/test_andrea/testFile_more_1GB163',
#            'root://xdpmhn01.tsy.cern.ch/dpm/tsy.cern.ch/home/dteam/test_andrea/testFile_more_1GB164',
#            'root://xdpmhn01.tsy.cern.ch/dpm/tsy.cern.ch/home/dteam/test_andrea/testFile_more_1GB165',
#            'root://xdpmhn01.tsy.cern.ch/dpm/tsy.cern.ch/home/dteam/test_andrea/testFile_more_1GB167',
#            'root://xdpmhn01.tsy.cern.ch/dpm/tsy.cern.ch/home/dteam/test_andrea/testFile_more_1GB17',
#            'root://xdpmhn01.tsy.cern.ch/dpm/tsy.cern.ch/home/dteam/test_andrea/testFile_more_1GB170',
#            'root://xdpmhn01.tsy.cern.ch/dpm/tsy.cern.ch/home/dteam/test_andrea/testFile_more_1GB177',
#            'root://xdpmhn01.tsy.cern.ch/dpm/tsy.cern.ch/home/dteam/test_andrea/testFile_more_1GB178',
#            'root://xdpmhn01.tsy.cern.ch/dpm/tsy.cern.ch/home/dteam/test_andrea/testFile_more_1GB179',
#            'root://xdpmhn01.tsy.cern.ch/dpm/tsy.cern.ch/home/dteam/test_andrea/testFile_more_1GB180',
#            'root://xdpmhn01.tsy.cern.ch/dpm/tsy.cern.ch/home/dteam/test_andrea/testFile_more_1GB181',
#            'root://xdpmhn01.tsy.cern.ch/dpm/tsy.cern.ch/home/dteam/test_andrea/testFile_more_1GB182',
#            'root://xdpmhn01.tsy.cern.ch/dpm/tsy.cern.ch/home/dteam/test_andrea/testFile_more_1GB183',
#            'root://xdpmhn01.tsy.cern.ch/dpm/tsy.cern.ch/home/dteam/test_andrea/testFile_more_1GB184']
